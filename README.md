## Getting started with SNAP - debian

### Using Binder

Click the badge below to run the notebooks on Binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/terradue-ogctb16%2Feoap%2Fd169-jupyter-nb%2Feo-processing-snap-debian/master?filepath=snap-demo.ipynb&urlpath=lab)

### Run locally using docker

Clone this repository with:

```bash
git clone https://gitlab.com/terradue-ogctb16/eoap/d169-jupyter-nb/eo-processing-snap-debian.git
```

Go to the directory containing the cloned repository:

```bash
cd eo-processing-snap-debian
```

Use `docker-compose` to build the docker image:

```bash
docker-compose build
```

This step can take a few minutes...

Finally run the docker with:

```
docker-compose up
```

Open a browser window at the address http://0.0.0.0:9005 to access the Jupyter Lab environment and SNAP


## Getting the EO data

Open a new terminal in the Jupyter Lab and do:

```bash
mkdir -p ~/data
```

#### Sentinel-1

```bash
cd ~/data
nextcloudcmd --user 'sentinel-1' --password 'sentinel-1' . https://nx13206.your-storageshare.de
```

## Using gpt via terminal

Open a new terminal in the Jupyter Lab environment and access gpt with:

```bash
/srv/conda/envs/env_snap/snap/bin/gpt -h
```



